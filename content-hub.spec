Name:   		content-hub
Version:    	r1507
Release:    	1%{?dist}
Summary:    	Content sharing/picking infrastructure and service

License:    	LGPL3
URL:    		https://github.com/ubports/content-hub
Source0:	
Patch1: 		GTest.patch
Patch2: 		QtArg.patch
Patch3: 		GtestSrc.patch

BuildRequires:	cmake
BuildRequires:  cmake-extras
BuildRequires:  doxygen
BuildRequires:  graphviz
Requires:   	lcov
Requires:   	gcovr
Requires:   	gmock
Requires:   	ubuntu-app-launch
Requires:   	ubuntu-download-manager
Requires:   	gsettings-qt
Requires:   	apparmor-libapparmor
Requires:   	libnotify
Requires:   	ubuntu-ui-toolkit
Requires:   	xorg-server-xvfb

BuildArch:	x86_64

%description
Content-hub is a mediation service to let applications share content between them even if they are not running at the same time

%prep
%setup -q
%patch1 -p1
%patch2 -p1
%patch3 -p1


%build
%cmake .
make %{?_smp_mflags}

%check
make ARGS+="--output-on-failure" test


%install
%make_install

%find_lang %{name}

%files -f %{name}.lang
%doc %{_datadir}/doc/%{name}/cpp/html/
%doc %{_datadir}/doc/%{name}/cpp/html/search/

%{_bindir}/%{name}-*

%{_includedir}/com/ubuntu/content/*.h
%{_includedir}/com/ubuntu/content/glib/%{name}-glib.h

%{_libdir}/lib%{name}*
%{_libdir}/pkgconfig/lib%{name}*
%{_libdir}/qt/qml/Ubuntu/Content/*
%{_libdir}/systemd/user/%{name}.service
%{_libdir}/ubuntu-app-launch/%{name}/exec-tool

%{_datadir}/glib-2.0/schemas/com.ubuntu.content.hub.gschema.xml
%{_datadir}/dbus-1/services/com.ubuntu.content.dbus.Service.service
%{_datadir}/icons/hicolor/512x512/apps/%{name}-*.png
%{_datadir}/%{name}/peers/%{name}-test-*
%{_datadir}/%{name}/testability/data/*
%{_datadir}/applications/%{name}-*.desktop
%{_datadir}/url-dispatcher/urls/%{name}-send.url-dispatcher



%changelog
* Sat Sep 29 2018 Abhishek Mudgal <abhishek.mudgal.59@gmail.com>
- Create a spec file
